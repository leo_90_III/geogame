﻿using UnityEngine;
using System.Collections;
using Assets.Resources.Code.Scripts;

/// <summary>
/// player class
/// </summary>
public class Shooter : MonoBehaviour
{
    /// <summary>
    /// hold a reference to a physics controlled object we will make
    /// </summary>
    public Rigidbody bullet;
    /// <summary>
    /// use to set the power of shooting. default 1500
    /// </summary>
    public float power = 1500f;
    /// <summary>
    /// define the speed of movement of the camera 
    /// </summary>
    public float moveSpeed = 2f;
    /// <summary>
    /// reference to active camera
    /// </summary>
    public Camera myCamera;
    /// <summary>
    /// reference to data of game
    /// </summary>
    private GameData gameDataRef;
    /// <summary>
    /// used to reference to itself
    /// </summary>
    private static Shooter matShooter;

    /// <summary>
    /// Use this for initialization
    /// </summary>
	void Start () {

        // assign GameData to gameDataRef variable
        gameDataRef = GameObject.Find("GameManager").GetComponent<GameData>();

        // reference to itself
        matShooter = this;
	}

    /// <summary>
    /// Update shooter object
    /// </summary>
    /// <param name="fireState">true if I can fire</param>
	public void UpdateShooter ( bool fireState ) {

        // if fire state =  true shoot a sphere
        if (fireState)
        {
            // transform mouse position form screen to camera
            Ray ray = myCamera.ScreenPointToRay(Input.mousePosition);

            // drawing green line from ray orignin, in ray deirection, of force value
            Debug.DrawRay(ray.origin, ray.direction * gameDataRef.forceManager.actualForce, Color.green);

            //instantiate a new sphere assign it to bullet and positioning it in the scene
            Rigidbody sphereInstance = Instantiate(bullet, ray.origin, transform.rotation) as Rigidbody;

            // reference to the direction forward of camera ( really to the object which the script is attached and in our case camera )
            Vector3 forwardCameraDirection = transform.TransformDirection(Vector3.forward);

            Debug.LogWarning("Fire Force value:= " + gameDataRef.forceManager.actualForce);
            // adding force to the sphere instance
            sphereInstance.AddForce(ray.direction * gameDataRef.forceManager.actualForce);
        }
	}
}